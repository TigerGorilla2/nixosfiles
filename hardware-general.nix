{ lib, inputs, system, hostname, ... }:

let
  luksd-uuid = "/dev/mapper/luksd-hub";
  gaming-uuid = "/dev/disk/by-uuid/5eec6dfd-a1f3-4934-904a-95785327c37f";
  backup-1T-uuid = "/dev/disk/by-uuid/fc4094b6-e423-4fd7-a2fd-5e2a984d29c2";
  laptop-backup-uuid = "/dev/disk/by-uuid/5d36011a-8d3b-4c32-bc74-b6a8742c4aa2";

  rudi-boot-uuid = "/dev/disk/by-uuid/B2C8-40D2";
  rudi-ventoy-uuid = "/dev/disk/by-uuid/3606-6BA1";
  sandisk-mini-128G-uuid = "/dev/disk/by-uuid/6449b4af-bd75-44d2-ab06-c049a2eb95f2";
  sandisk-dual-uuid = "/dev/disk/by-uuid/4383-5041";

  enableGaming = lib.elem hostname [ "david-pc" ];
in {
  imports = [
    inputs.nixos-hardware.nixosModules.common-pc-ssd
  ];

  nixpkgs.hostPlatform = lib.mkDefault system;
  hardware.enableAllFirmware = true;

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.efi.efiSysMountPoint = "/efi";

  services.btrfs.autoScrub = {
    enable = true;
    interval = "weekly";
    fileSystems = [
      luksd-uuid
      backup-1T-uuid
      laptop-backup-uuid
    ] ++ lib.optionals enableGaming [
      gaming-uuid
    ];
  };

  ##################################################
  ### FILESYSTEMS
  ##################################################
  fileSystems."/" = {
    device = luksd-uuid;
    fsType = "btrfs";
    options = [ "compress=zstd" "subvol=/nixos/@root" ];
  };

  fileSystems."/home" = {
    device = luksd-uuid;
    fsType = "btrfs";
    options = [ "compress=zstd" "subvol=/nixos/@home" ];
  };

  fileSystems."/nix" = {
    device = luksd-uuid;
    fsType = "btrfs";
    options = [ "noatime" "compress=zstd" "subvol=/nixos/@nix" ];
  };

  fileSystems."/var" = {
    device = luksd-uuid;
    fsType = "btrfs";
    options = [ "compress=zstd" "subvol=/nixos/@var" ];
  };

  fileSystems."/mnt/hub" = {
    device = luksd-uuid;
    fsType = "btrfs";
    options = [ "ro" "noauto" ];
  };

  ### Swap
  fileSystems."/swap" = {
    device = luksd-uuid;
    fsType = "btrfs";
    options = [ "noatime" "subvol=/swap" ];
  };

  swapDevices = [ { device = "/swap/swapfile"; } ];

  ### Userland
  fileSystems."/dbx-home" = {
    device = luksd-uuid;
    fsType = "btrfs";
    options = [ "compress=zstd" "subvol=/dbx-home" ];
  };

  fileSystems."/dbx-home/home/stuff" = {
    device = luksd-uuid;
    fsType = "btrfs";
    options = [ "exec" "compress=zstd" "subvol=/stuff" ];
  };

  fileSystems."/home/david/stuff" = {
    device = luksd-uuid;
    fsType = "btrfs";
    options = [ "exec" "compress=zstd" "subvol=/stuff" ];
  };

  fileSystems."/dbx-home/home/gaming" = lib.mkIf enableGaming {
    device = gaming-uuid;
    fsType = "btrfs";
    options = [ "exec" "compress=zstd" ];
  };

  fileSystems."/home/david/gaming" = lib.mkIf enableGaming {
    device = gaming-uuid;
    fsType = "btrfs";
    options = [ "exec" "compress=zstd" ];
  };

  ### Userland Tmp
  fileSystems."/dbx-home/home/tmp" = {
    device = "none";
    fsType = "tmpfs";
    options = [ "size=4G" "mode=0700" "uid=david" "gid=users" ];
  };


  ##################################################
  ### EXTRA DEVICES
  ##################################################
  fileSystems."/mnt/david/stuff" = {
    device = luksd-uuid;
    fsType = "btrfs";
    options = [ "noauto" "user" "compress=zstd" "subvol=/stuff" ];
  };

  fileSystems."/mnt/david/gaming" = {
    device = gaming-uuid;
    fsType = "btrfs";
    options = [ "noauto" "user" "compress=zstd" ];
  };

  # note: samsung-850-250G is used for gaming on pc (raid0)
  # note: samsung-870-250G is used for gaming on pc (raid0)
  # note: samsung-860-1T is used for backups on pc
  # note: kingston-250G is used for backups on laptop
  fileSystems."/mnt/david/atom-samsung-850-250G" = {
    device = gaming-uuid;
    fsType = "btrfs";
    options = [ "noauto" "user" ];
  };

  fileSystems."/mnt/david/body-samsung-860-1T" = {
    device = backup-1T-uuid;
    fsType = "btrfs";
    options = [ "noauto" "user" "compress=zstd" ];
  };

  fileSystems."/mnt/david/crib-kingston-250G" = {
    device = laptop-backup-uuid;
    fsType = "btrfs";
    options = [ "noauto" "user" "compress=zstd" "subvol=/" ];
  };

  fileSystems."/mnt/david/crib-kingston-250G_backup" = {
    device = laptop-backup-uuid;
    fsType = "btrfs";
    options = [ "noauto" "user" "compress=zstd" "subvol=/backup" ];
  };

  fileSystems."/mnt/david/dibs-samsung-870-250G" = {
    device = gaming-uuid;
    fsType = "btrfs";
    options = [ "noauto" "user" ];
  };

  ### Hotplug
  fileSystems."/mnt/david/rudi-boot" = {
    device = rudi-boot-uuid;
    fsType = "vfat";
    options = [ "noauto" "user" ];
  };

  fileSystems."/mnt/david/rudi-ventoy" = {
    device = rudi-ventoy-uuid;
    fsType = "exfat";
    options = [ "noauto" "user" ];
  };

  fileSystems."/mnt/david/womb-mini-sandisk-128G_backup" = {
    device = sandisk-mini-128G-uuid;
    fsType = "btrfs";
    options = [ "noauto" "user" "compress=zstd" "subvol=/backup" ];
  };

  fileSystems."/mnt/david/womb-mini-sandisk-128G_storage" = {
    device = sandisk-mini-128G-uuid;
    fsType = "btrfs";
    options = [ "noauto" "user" "compress=zstd" "subvol=/storage" ];
  };

  fileSystems."/mnt/david/yard-dual-sandisk-seedvault" = {
    device = sandisk-dual-uuid;
    fsType = "vfat";
    options = [ "noauto" "user" ];
  };
}
