{
  description = "A simple NixOS flake";

  inputs = {
    # NixOS official package source, using the nixos-unstable branch here
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.11";
    nixpkgs.follows = "nixos-cosmic/nixpkgs-stable"; # to sync up with cachix for cosmic
    nixpkgs-unstable.url = "github:NixOS/nixpkgs/nixos-unstable";

    # See nixos-hardware/flake.nix for a list of available modules
    nixos-hardware.url = "github:NixOS/nixos-hardware";
    nixos-cosmic.url = "github:lilyinstarlight/nixos-cosmic";
    home-manager.url = "github:nix-community/home-manager/release-24.11";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = inputs@{ self, nixpkgs, nixpkgs-unstable, nixos-hardware, home-manager, nixos-cosmic, ... }: let
    enable-flakes = [
      {
        nix.settings.experimental-features = [ "nix-command" "flakes" ];
        nix.registry.nixpkgs.flake = nixpkgs;
      }
    ];

    mkHost = hostname: system: other:
      nixpkgs.lib.nixosSystem {
        system = system;
        specialArgs = { inherit inputs system hostname; };
        modules = enable-flakes ++ other.modules;
      } // other;
  in {
    nixosConfigurations.david-pc = mkHost "david-pc" "x86_64-linux" {
      modules = [
        ./pc.nix
        ./general.nix
        ./hardware-general.nix
        ./hardware-network.nix
        ./users.nix

        ./cosmic.nix
        ./sway.nix
      ];
    };

    nixosConfigurations.david-laptop = mkHost "david-laptop" "x86_64-linux" {
      modules = [
        ./laptop.nix
        ./general.nix
        ./hardware-general.nix
        ./hardware-network.nix
        ./users.nix

        ./cosmic.nix
        ./sway.nix
      ];
    };
  };
}
