{ config, lib, pkgs, ... }:

let
  nvidiaEnabled = lib.elem "nvidia" config.services.xserver.videoDrivers;
in {
  programs.sway = {
    enable = true;
    extraOptions = lib.mkIf nvidiaEnabled [
      "--unsupported-gpu"
    ];
    extraPackages = [
      pkgs.foot
      pkgs.kdePackages.polkit-qt-1
      pkgs.swaylock
    ];
  };

  services.power-profiles-daemon.enable = true;

  # NOTE: broken on sway with a stock install, maybe needs some exports to
  #       systemd-env or something
  # programs.gnupg.agent.pinentryPackage = pkgs.pinentry-qt;
}
