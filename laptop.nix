{ lib, pkgs, inputs, ... }:

{
  imports = [
    inputs.nixos-hardware.nixosModules.common-cpu-intel
    inputs.nixos-hardware.nixosModules.common-gpu-intel
    inputs.nixos-hardware.nixosModules.common-pc-laptop
  ];

  ##################################################
  ### HARDWARE
  ##################################################
  boot.initrd.availableKernelModules = [ "xhci_pci" "ahci" "nvme" "usbhid" "usb_storage" "sd_mod" "sdhci_pci" ];
  boot.initrd.kernelModules = [ ];

  boot.kernelPackages = pkgs.linuxPackages_latest;
  boot.kernelModules = [
    "kvm-intel"
    "i2c-dev" # external monitor brightness
  ];
  boot.extraModulePackages = [ ];
  boot.kernelParams = [ ];

  ##################################################
  ### FILESYSTEMS (rest is in general-hardware.nix)
  ##################################################
  boot.initrd.luks.devices."luksd-hub".device = "/dev/disk/by-uuid/d961afbf-9c23-4e77-b2c1-c936bcc76e5a";

  fileSystems."/efi" = {
    device = "/dev/disk/by-uuid/B444-BDC4";
    fsType = "vfat";
    options = [ "fmask=0022" "dmask=0022" ];
  };
}
