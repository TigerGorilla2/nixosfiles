{ config, lib, pkgs, inputs, ... }:

{
  imports = [
    inputs.nixos-hardware.nixosModules.common-cpu-amd
    inputs.nixos-hardware.nixosModules.common-cpu-amd-pstate
    inputs.nixos-hardware.nixosModules.common-gpu-amd

    #inputs.nixos-hardware.nixosModules.common-gpu-nvidia # this has prime offloading
    #inputs.nixos-hardware.nixosModules.common-gpu-nvidia-noprime
    inputs.nixos-hardware.nixosModules.common-gpu-nvidia-sync
    (inputs.nixos-hardware + "/common/gpu/nvidia/pascal")
  ];

  ##################################################
  ### HARDWARE
  ##################################################
  # https://nixos.wiki/wiki/Nvidia
  hardware.nvidia.prime = {
    # 0000:08:00 -> 8:0:0
    amdgpuBusId = "PCI:8:0:0";
    # 0000:01:00 -> 1:0:0
    nvidiaBusId = "PCI:1:0:0";
  };
  services.udev.extraRules = ''
    KERNEL=="card*", SUBSYSTEM=="drm", ATTRS{vendor}=="0x1002", ATTRS{device}=="0x1638", SYMLINK+="dri/by-name/apu-amd"
    KERNEL=="card*", SUBSYSTEM=="drm", ATTRS{vendor}=="0x10de", ATTRS{device}=="0x1c82", SYMLINK+="dri/by-name/gpu-nvidia"
  '';
  hardware.nvidia-container-toolkit.enable = true; # for distrobox

  specialisation.no-prime.configuration = {
    hardware.nvidia.prime.offload.enable = lib.mkForce false;
    hardware.nvidia.prime.offload.enableOffloadCmd = lib.mkForce false;
    hardware.nvidia.prime.reverseSync.enable = lib.mkForce false;
    hardware.nvidia.prime.sync.enable = lib.mkForce false;
  };

  specialisation.prime-offload.configuration = {
    hardware.nvidia.prime.offload.enable = lib.mkForce true;
    hardware.nvidia.prime.offload.enableOffloadCmd = lib.mkForce true;
    hardware.nvidia.prime.reverseSync.enable = lib.mkForce false;
    hardware.nvidia.prime.sync.enable = lib.mkForce false;
  };

  boot.initrd.availableKernelModules = [ "xhci_pci" "ahci" "nvme" "usb_storage" "usbhid" "sd_mod" ];
  boot.initrd.kernelModules = [ ];

  # FIXME: nvidia 565 broken on kernel 6.13
  boot.kernelPackages = pkgs.linuxPackages_latest;
  hardware.nvidia.package = config.boot.kernelPackages.nvidiaPackages.mkDriver {
    version = "570.124.04";
    sha256_64bit =   "sha256-G3hqS3Ei18QhbFiuQAdoik93jBlsFI2RkWOBXuENU8Q=";
    sha256_aarch64 = "sha256-BAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=";
    openSha256 =     "sha256-CAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=";
    settingsSha256 = "sha256-LNL0J/sYHD8vagkV1w8tb52gMtzj/F0QmJTV1cMaso8=";
    persistencedSha256 = lib.fakeSha256;
  };
  #boot.kernelPackages = pkgs.linuxPackages_6_12;
  boot.kernelModules = [
    "kvm-amd"
    "nct6775"
    "i2c-dev" # external monitor brightness
  ];
  boot.blacklistedKernelModules = [
    "nouveau"
    # "snd_hda_intel"
  ];
  boot.extraModprobeConfig = "
    options vfio_pci ids=10de:1c82,10de:0fb9
  ";
  boot.extraModulePackages = [ ];
  boot.kernelParams = [
    "iommu=pt" "fbcom=map:11110"
  ];

  ##################################################
  ### FILESYSTEMS (rest is in general-hardware.nix)
  ##################################################
  boot.initrd.luks.devices."luksd-hub".device = "/dev/disk/by-uuid/b47f661b-8fcc-4394-ab5f-c5fccb644b68";

  fileSystems."/efi" = {
    device = "/dev/disk/by-uuid/A887-4748";
    fsType = "vfat";
    options = [ "fmask=0022" "dmask=0022" ];
  };

  ##################################################
  ### MISC
  ##################################################
  # Virtualisation
  virtualisation.libvirtd.enable = true;
  users.users."david".extraGroups = [ "libvirtd" ];
  environment.etc."tmpfiles.d/10-looking-glass.conf".text = ''
# https://looking-glass.io/docs/B6/install/#permissions
# Type Path               Mode UID  GID Age Argument
f /dev/shm/looking-glass 0660 david kvm -
  '';

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;
  services.openssh.settings = {
    PermitRootLogin = "no";
    PasswordAuthentication = false;
  };
}
