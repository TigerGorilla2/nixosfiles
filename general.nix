{ config, lib, pkgs, inputs, system, hostname, ... }:

{
  nixpkgs.config.allowUnfree = true;
  nixpkgs.overlays = import ./overlays.nix {
    pkgs-unst = import inputs.nixpkgs-unstable { inherit system; };
  };

  # Set your time zone.
  time.timeZone = "Europe/Berlin";

  networking.hostName = hostname;
  networking.networkmanager.enable = true;
  networking.useDHCP = lib.mkDefault true;
  # networking.interfaces.enp3s0.useDHCP = lib.mkDefault true;
  # networking.interfaces.enp6s0.useDHCP = lib.mkDefault true;
  # networking.interfaces.wlp5s0.useDHCP = lib.mkDefault true;

  # Enable firewall, specific ports are opened in ./users.nix
  networking.firewall.enable = true;
  # services.opensnitch.enable = true;

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";
  i18n.supportedLocales = [
    "C.UTF-8/UTF-8"
    "en_US.UTF-8/UTF-8"
    "de_DE.UTF-8/UTF-8"
  ];
  console = {
    font = "Lat2-Terminus16";
    keyMap = "us";
  };

  documentation = {
    dev.enable = true;
    man.generateCaches = true;
    # NOTE: home-manager uses man-db
    #man.man-db.enable = false;
    #man.mandoc.enable = true;
  };

  # Clean /tmp and others (/etc/tmpfiles.d/*.conf) on boot
  boot.tmp.cleanOnBoot = true;

  nix.gc.automatic = true;
  nix.gc.options = "--delete-older-than 14d";

  # Mount etc onto etc (or something like that, i'd imagine)
  #system.etc.overlay.enable = true;
  #system.etc.overlay.mutable = false;

  system.autoUpgrade = {
    enable = true;
    flake = inputs.self.outPath;
    #flake = "gitlab:TigerGorilla2/nixosfiles";
    operation = "boot";
    flags = [
      #"--refresh"
      "--recreate-lock-file" # update everything # FIXME: this is deprecated
      #"--no-write-lock-file"
      "--print-build-logs"
    ];
    dates = "14:30";
  };
  services.fwupd.enable = true;

  security.apparmor.enable = true;
  security.chromiumSuidSandbox.enable = true;

  # Enable PolKit
  security.polkit.enable = true;

  services.dbus.implementation = "broker";

  # Enable sound.
  services.pipewire.enable = true;
  services.pipewire.pulse.enable = true;

  # Enable touchpad support (enabled default in most desktopManager).
  services.libinput.enable = true;
  hardware.trackpoint.enable = false;

  environment.shellInit = ''
    # https://felipec.wordpress.com/2021/06/05/adventures-with-man-color/
    # 172 is orange, b is blue
    export MANPAGER='less -R --use-color -Dd+172 -Du+b'
    export MANROFFOPT='-c'
  '';

  ##################################################
  ### PROGRAMS
  ##################################################
  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = [
    pkgs.bat
    pkgs.distrobox
    pkgs.man-pages
    pkgs.man-pages-posix

    (pkgs.writeShellApplication {
      name = "flake-rebuild";
      runtimeInputs = [ pkgs.jq ];
      text = ''
        flake_update=1
        if [ $# -gt 0 ]; then
          if [ "$1" = "help" ]; then
            echo "usage: $0 [-nu|--noupdate] <nixos-rebuild flake-options>"
            exit 0
          fi
          if [ "$1" = "-nu" ] || [ "$1" = "--noupdate" ]; then
            flake_update=0
          fi
        fi
        if [ "$flake_update" = "1" ]; then
          nix flake update --flake /etc/nixos
        fi
        nixos-rebuild --verbose --flake /etc/nixos boot
	echo FIXME: vvv this is outdated vvv
        echo -e "\nYou SHOULD now reboot and test the changes AND commit&push" \
            "to make them be used on ~~autoUpgrades~~!"
	echo FIXME: ^^^ this is outdated ^^^
      '';
    })

    pkgs.nix-weather
    (pkgs.writeShellApplication {
      name = "weather";
      runtimeInputs = [ pkgs.nix-weather ];
      text = ''nix-weather --config /etc/nixos/ --name "$(host)"'';
    })
  ];

  programs.vim.enable = true;
  programs.vim.defaultEditor = true;

  programs.git = {
    enable = true;
    config = {
      alias = {
        si = "switch";
        co = "checkout";
        br = "branch";

        pul = "pull";
        pom = "pull origin main";
        psh = "push";

        au = "add --update";
        aup = "add --update --patch";
        cm = "commit --message";

        st = "status";
        di = "diff";
        ds = "diff --staged";
        ls = "ls-files";

        unstage = "reset HEAD --";
        l1 = "log -1";
        tree = "log --all --decorate --oneline --graph";
      };
    };
  };

  programs.gnupg.agent.enable = true;
  programs.gnupg.agent.enableSSHSupport = true;

  # Copy the NixOS configuration file and link it from the resulting system
  # (/run/current-system/configuration.nix). This is useful in case you
  # accidentally delete configuration.nix.
  # NOTE: totally useless, because it copies a symlink (and also only
  #   configuration.nix)
  #system.copySystemConfiguration = true;
  # NOTE: this actually references the currently booted nixos configuration
  #       (including the active flake.lock)
  environment.etc."current-nixos".source = inputs.self.outPath;

  # This option defines the first version of NixOS you have installed on this particular machine,
  # and is used to maintain compatibility with application data (e.g. databases) created on older NixOS versions.
  #
  # Most users should NEVER change this value after the initial install, for any reason,
  # even if you've upgraded your system to a new NixOS release.
  #
  # This value does NOT affect the Nixpkgs version your packages and OS are pulled from,
  # so changing it will NOT upgrade your system - see https://nixos.org/manual/nixos/stable/#sec-upgrading for how
  # to actually do that.
  #
  # This value being lower than the current NixOS release does NOT mean your system is
  # out of date, out of support, or vulnerable.
  #
  # Do NOT change this value unless you have manually inspected all the changes it would make to your configuration,
  # and migrated your data accordingly.
  #
  # For more information, see `man configuration.nix` or https://nixos.org/manual/nixos/stable/options#opt-system.stateVersion .
  system.stateVersion = "24.05"; # Did you read the comment?
}
