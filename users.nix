{ config, lib, pkgs, inputs, ... }:

let
  nvidiaEnabled = lib.elem "nvidia" config.services.xserver.videoDrivers;
in {
  imports = [
    inputs.home-manager.nixosModules.home-manager
  ];

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.david = {
    isNormalUser = true;
    description = "David Klamroth";
    extraGroups = [
      "wheel" # Enable ‘sudo’ for the user.
      "dialout" # Access to /dev/ttyUSB0 and similar
      "adbusers" # adb, obviously
      "wireshark"
    ];
  };

  home-manager.useGlobalPkgs = true;
  home-manager.useUserPackages = true;
  home-manager.users.david = { config, pkgs, ... }: {
    programs.bash.enable = true;
    programs.bash.shellAliases."dbx" = "distrobox";
    programs.bash.shellAliases."home" = "distrobox enter --no-workdir home";
    programs.bash.profileExtra = ''
      # Start (preload) the home-box
      distrobox enter home -- true &
    '';

    services.opensnitch-ui.enable = true;
    services.syncthing.enable = true;
    services.syncthing.tray.enable = true;

    systemd.user.services."add-flathub" = {
      Unit.Description = "Automatically add flathub to users remotes.";
      Service.Exec = ''
        ${pkgs.flatpak}/bin/flatpak --user remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo
    '';
      Install.WantedBy = [ "default.target" ];
    };

    home.packages = [
      (pkgs.writeShellScriptBin "home" ''distrobox enter --no-workdir --name home "$@"'')
      pkgs.flatpak # and also dbx-host-exec (host-spawn)
      pkgs.unst.nerd-fonts.dejavu-sans-mono
      pkgs.unst.nerd-fonts.fira-code
      pkgs.unst.nerd-fonts.roboto-mono
      pkgs.unst.noto-fonts-emoji
    ];

    home.file.".config/cosmic".source = config.lib.file.mkOutOfStoreSymlink "/dbx-home/home/.config/cosmic";
    home.file.".config/foot".source = config.lib.file.mkOutOfStoreSymlink "/dbx-home/home/.config/foot";
    home.file.".config/mimeapps.list".source =
      config.lib.file.mkOutOfStoreSymlink "/dbx-home/home/.config/mimeapps.list";
    home.file.".config/sway".source = config.lib.file.mkOutOfStoreSymlink "/dbx-home/home/.config/sway";
    home.file.".config/swaylock".source =
      config.lib.file.mkOutOfStoreSymlink "/dbx-home/home/.config/swaylock";

    home.file."Documents".source = config.lib.file.mkOutOfStoreSymlink "/dbx-home/home/Documents";
    home.file."Downloads".source = config.lib.file.mkOutOfStoreSymlink "/dbx-home/home/Downloads";
    home.file."Music".source = config.lib.file.mkOutOfStoreSymlink "/dbx-home/home/Music";
    home.file."Pictures".source = config.lib.file.mkOutOfStoreSymlink "/dbx-home/home/Pictures";
    home.file."Public".source = config.lib.file.mkOutOfStoreSymlink "/dbx-home/home/Public";
    home.file."Templates".source = config.lib.file.mkOutOfStoreSymlink "/dbx-home/home/Templates";
    home.file."Videos".source = config.lib.file.mkOutOfStoreSymlink "/dbx-home/home/Videos";

    home.stateVersion = "24.05";
  };

  networking.firewall.enable = true;
  services.opensnitch.enable = true;
  # Open ports in the firewall.
  networking.firewall.allowedTCPPorts = [ ];
  networking.firewall.allowedUDPPorts = [
    24727 # AusweisApp
  ];

  # Also used for printing
  services.avahi = {
    enable = true;
    nssmdns4 = true;
    openFirewall = true;
  };

  # Enable CUPS to print documents.
  services.printing.enable = true;
  services.printing.drivers = [ pkgs.brlaser pkgs.cups-brother-hl1210w ];

  hardware.printers.ensurePrinters = [
    {
      name = "Brother-HL-1210W";
      location = "Am Bad";
      deviceUri = "ipp://brother1210w";
      model = "drv:///brlaser.drv/br1200.ppd";
      ppdOptions = {
        PageSize = "A4";
      };
    }
  ];

  # Flatpak
  services.flatpak.enable = true;

  # For distrobox
  virtualisation.podman.enable = true;
  # services.flatpak.enable = true; # dbx-host-exec (host-spawn) (defined above (in packages))

  # Check if i2c is needed in nixos
  hardware.i2c.enable = true;
  services.udisks2.enable = true;

  # Just open the ports, syncthing is enabled in home-manager
  services.syncthing = {
    enable = true;
    systemService = false;
    openDefaultPorts = true;
  };

  programs.adb.enable = true; # for adbusers group & udev rules
  programs.wireshark.enable = true;
  programs.wireshark.package = pkgs.wireshark;

  services.udev.extraRules = ''
    # Steam-input Controller write access
    KERNEL=="uinput", SUBSYSTEM=="misc", TAG+="uaccess", OPTIONS+="static_node=uinput"
    # Wheel: Logitech WingMan Formula GP
    KERNEL=="hidraw*", ATTRS{idVendor}=="046d", ATTRS{idProduct}=="c20e", MODE="0660", TAG+="uaccess"
  '';

  security.wrappers."mount.nfs" = {
    setuid = true;
    source = "${pkgs.nfs-utils}/bin/mount.nfs";
    owner = "root";
    group = "root";
  };

  security.wrappers."mount.cifs" = {
    setuid = true;
    source = "${pkgs.cifs-utils}/bin/mount.cifs";
    owner = "root";
    group = "root";
  };
}
