{ pkgs-unst, ... }:

let
  addPatches = pkg: patches:
    pkg.overrideAttrs (oldAttrs: { patches = (oldAttrs.patches or [ ]) ++ patches; } );
in [
  (final: prev: {
    unst = pkgs-unst;
    # NOTE: Remove once distrobox 1.81 is out and use pkgs.unst.distrobox then (in ./general.nix)
    # 1.80 (which is in stable) is slow!
    # 1.81 is out in unst, using that now
    #distrobox = addPatches prev.distrobox [ ./even-faster-distrobox-enter.patch ];
    distrobox = final.unst.distrobox;
  })
]
