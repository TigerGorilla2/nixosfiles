{ ... }:

let
  nfsOptions = {
    fsType = "nfs";
    options = [
      "noauto"
      "users"
      "x-systemd.idle-timeout=600"
    ];
  };

  davfsOptions = {
    fsType = "davfs";
    options = [ "noauto" "users" ];
  };

  cifsOptions = {
    fsType = "cifs";
    options = [ "uid=david" "noauto" "user" ];
  };
  cifsDavidOptions = cifsOptions // {
    fsType = "cifs";
    options = [ "uid=david" "noauto" "user" "user=david" ];
  };
in {
  services.davfs2.enable = true;

  # NFS (mounted in home)
  fileSystems."/dbx-home/home/nfs" = nfsOptions // {
    device = "serverathome:/mnt/nas/unix-share";
  };
  fileSystems."/dbx-home/home/nfs/sync" = nfsOptions // {
    device = "serverathome:/mnt/nas/syncthing-david";
  };

  # NAS
  fileSystems."/mnt/david/nas/webdav" = davfsOptions // {
    device = "https://serverathome:30035/webdav";
  };
  fileSystems."/mnt/david/nas/apt-repo" = davfsOptions // {
    device = "https://serverathome:30035/apt-repo";
  };
  fileSystems."/mnt/david/nas/syncthing-webdav" = davfsOptions // {
    device = "https://serverathome:30035/syncthing";
  };
  fileSystems."/mnt/david/nas/smb" = cifsDavidOptions // {
    device = "//serverathome/nas-smb";
  };

  # BananaPi
  fileSystems."/mnt/david/bpi/david" = cifsDavidOptions // {
    device = "//bananapi/david";
  };
  fileSystems."/mnt/david/bpi/klami" = cifsDavidOptions // {
    device = "//bananapi/klami/public";
  };
  fileSystems."/mnt/david/bpi/julia" = cifsDavidOptions // {
    device = "//bananapi/julia/public";
  };
  fileSystems."/mnt/david/bpi/ruben" = cifsDavidOptions // {
    device = "//bananapi/ruben/public";
  };
  fileSystems."/mnt/david/bpi/speicher" = cifsDavidOptions // {
    device = "//bananapi/speicher";
  };
  fileSystems."/mnt/david/bpi/filme" = cifsOptions // {
    device = "//bananapi/filme";
  };
}
