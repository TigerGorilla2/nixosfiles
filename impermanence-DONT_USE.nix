# NOTE: DONT use this for now. some things are missing and I cba to figure what it is
{ config, lib, ... }:

{
  ##################################################
  ### Filesystem things
  ##################################################
  boot.initrd.postDeviceCommands = lib.mkAfter ''
    mkdir /imp-roots
    mount /dev/mapper/cr-hub /imp-roots -o subvol=/nixos/impermanence-roots
    if [[ -e /imp-roots/root ]]; then
        mkdir -p /imp-roots/old
        timestamp=$(date --date="@$(stat -c %Y /imp-roots/root)" "+%Y-%m-%-d_%H:%M:%S")
        mv /imp-roots/root "/imp-roots/old/$timestamp"
    fi

    delete_subvolume_recursively() {
        IFS=$'\n'
        for i in $(btrfs subvolume list -o "$1" | cut -f 9- -d ' '); do
            delete_subvolume_recursively "/imp-roots/$i"
        done
        btrfs subvolume delete "$1"
    }

    for i in $(find /imp-roots/old/ -maxdepth 1 -mtime +30); do
        delete_subvolume_recursively "$i"
    done

    btrfs subvolume create /imp-roots/root
    umount /imp-roots
  '';

  fileSystems."/" = {
    device = "/dev/mapper/cr-hub";
    fsType = "btrfs";
    options = [ "compress=zstd" "subvol=/nixos/impermanence-roots/root" ];
  };

  fileSystems."/persistent" = {
    device = "/dev/mapper/cr-hub";
    fsType = "btrfs";
    options = [ "compress=zstd" "subvol=/nixos/impermanence-persistent" ];
  };

  ##################################################
  ### Impermanence configuration
  ##################################################
  environment.persistence."/persistent" = {
    hideMounts = true;
    directories = [
      "/etc/nixos/"

      "/etc/NetworkManager/system-connections/" # wifi connections

      "/var/log/"
      "/var/lib/bluetooth/"
      "/var/lib/nixos/"
      "/var/lib/systemd/coredump/"
      { directory = "/var/lib/colord/"; user = "colord"; group = "colord"; mode = "u=rwx,g=rx,o="; } # dunnu what for, but is in the example

      "/tmp/" # mostly to keep nix builds cached
      "/var/tmp/" # should just be preserved
    ];
    files = [
      "/etc/adjtime" # hardware clock offset
      "/etc/machine-id"
    ];
  };
}
